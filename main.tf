resource "yandex_vpc_network" "network" {
  name = "${var.network-name}"
}

# Создаем подсети
resource "yandex_vpc_subnet" "subnets" {
  for_each = var.zones

  name           = each.value["name"]
  zone           = each.value["zone"]
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = [each.value["cidr"]]
}

output "subnets" {
  description = "Созданные подсети"
  value       = yandex_vpc_subnet.subnets
}
