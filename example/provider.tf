terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      #version = "0.74"
    }
  }
}

provider "yandex" {
  cloud_id  = "b1g...bb"
  folder_id = "b1g...j8"
  zone      = "ru-central1-a"
}

