variable "network-name" {
  description = "Название сети"
  type        = string
  default     = "network"
}

variable "zones" {
  description = "Зоны, в которых будут расположены ресурсы"
  type        = map
  default = {
    zone_a = {
      name = "subnet-a"
      zone = "ru-central1-a"
      cidr = "10.130.0.0/24"
    }
    zone_b = {
      name = "subnet-b"
      zone = "ru-central1-b"
      cidr = "10.129.0.0/24"
    }
    zone_c = {
      name = "subnet-c"
      zone = "ru-central1-c"
      cidr = "10.128.0.0/24"
    }
  }
}
