module "network" {  
  source  = "git@gitlab.com:common/terraform-modules/network.git"
  network-name  = var.network-name
  zones         = var.zones
}

output "tmp" {
  value       = var.network-name
}
