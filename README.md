# Создает подсети в трех зонах яндекса 

Автор   : eshlygin (shen22@rambler.ru)

Создано : 2023.01.28

---

## Описание

Создает сеть и подсети с нужным названиями и параметрами (диапазон адресов)

Для работы потребуеются файлы:

* main.tf

* provider.tf

* variables.tf

* terraform.tfvars

При этом файл provider.tf можно сделать симлинком на общий файл где то в корне проекта.

Корректировать значения нужно только в terraform.tfvars

### main.tf

Подключает модуль из гита

```
module "network" {  
  source  = "git@gitlab.com:common/terraform-modules/network.git"
  network-name  = var.network-name
  zones         = var.zones
}
```

### provider.tf

Настраивает параметры провайдера и передает их в модуль. Можно сделать симлинком на один файл где нибудь в корне проекта

```
terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      #version = "0.74"
    }
  }
}

provider "yandex" {
  cloud_id  = "b1g...bb"
  folder_id = "b1g...j8"
  zone      = "ru-central1-a"
}
```

### variables.tf

Служит для того чтобы определить какие переменные должны быть переданы.

Копия файла из папки модуля. После успешного инита файл появится в подпапке:

```
.terraform/modules/network/variables.tf
```

Сейчас выглядит так:

```
variable "network-name" {
  description = "Название сети"
  type        = string
  default     = "network"
}

variable "zones" {
  description = "Зоны, в которых будут расположены ресурсы"
  type        = map
  default = {
    zone_a = {
      name = "subnet-a"
      zone = "ru-central1-a"
      cidr = "10.130.0.0/24"
    }
    zone_b = {
      name = "subnet-b"
      zone = "ru-central1-b"
      cidr = "10.129.0.0/24"
    }
    zone_c = {
      name = "subnet-c"
      zone = "ru-central1-c"
      cidr = "10.128.0.0/24"
    }
  }
}
```

### terraform.tfvars

Содержит нужные переменные

```
# название сети
network-name = "network"

# настройка подсетей в разных зонах
zones = {
  zone_a = {
    name = "subnet-a"
    zone = "ru-central1-a"
    cidr = "10.130.0.0/24"
  }
  zone_b = {
    name = "subnet-b"
    zone = "ru-central1-b"
    cidr = "10.129.0.0/24"
  }
  zone_c = {
    name = "subnet-c"
    zone = "ru-central1-c"
    cidr = "10.128.0.0/24"
  }
}
```
